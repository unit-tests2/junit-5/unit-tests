### Unit Tests

```
1. Mock static method

  - ref AppUtilUnitTest.java
  - ref AppUtilWrapperUnitTest.java
  - ref AppUtilWrapperWrapperUnitTest.java
  
2. Setting Target test class fields via ReflectionTestUtils

  - ref StudentServiceUnitTest.beforeEach
  
3. Custom Extenstions class that we used in @ExtendsWith
  - ref MyExtendsWithExtension.java
  - ref MyTestAuditLogExtension.java
  - ref MyTestExecutionTimeExtension.java
  
```