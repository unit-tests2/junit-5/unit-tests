package com.example.service.ReflectionTestUtils;

import com.example.base.MyExtendsWithExtension;
import com.example.dto.Student;
import com.example.repository.StudentRepository;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StudentServiceUnitTest implements MyExtendsWithExtension {

    @BeforeAll
    void beforeAll(){
        System.out.println("beforeAll........");
    }

    @AfterAll
    void afterAll(){
        System.out.println("afterAll........");
    }

    @BeforeEach
    void beforeEach(){
        System.out.println("beforeEach........");
        /*
        we need below statement because of below 2 root cause
        1.  studentRepository is final in StudentService
        AND
        2. StudentServiceUnitTest is annotated as @TestInstance(TestInstance.Lifecycle.PER_CLASS)

        Hence, for the ONE UNIT TEST (whichever runs FIRST)  mock will work
         */
        ReflectionTestUtils.setField(studentService, "studentRepository", mockStudentRepository);
    }
    @AfterEach
    void afterEach(){
        System.out.println("afterEach........");
    }
    @Mock
    private StudentRepository mockStudentRepository;
    @InjectMocks
    private StudentService studentService;


    @Nested
    class GetStudentByIdUnitTest {

        @Test
        void getStudentById_success_1(){
            // given
            long expectedStudentId = 1L;
            Student expected = new Student();
            expected.setId(expectedStudentId);
            expected.setFirstName("Kaushlendra");
            expected.setMidName("Singh");
            expected.setLastName("Chauhan");
            Mockito.when(mockStudentRepository.getStudentById(expectedStudentId))
                    .thenReturn(expected);
            Student actual = assertDoesNotThrow(() -> studentService.getStudentById(expectedStudentId));
            assertNotNull(actual);
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.getFirstName(), actual.getFirstName());
            assertEquals(expected.getLastName(), actual.getLastName());
            assertEquals(expected.getMidName(), actual.getMidName());
        }

        @Test
        void getStudentById_success_2(){
            // given
            long expectedStudentId = 2L;
            Student expected = new Student();
            expected.setId(expectedStudentId);
            expected.setFirstName("Kaushlendra");
            expected.setMidName("Singh");
            expected.setLastName("Chauhan");
            Mockito.when(mockStudentRepository.getStudentById(expectedStudentId))
                    .thenReturn(expected);
            Student actual = assertDoesNotThrow(() -> studentService.getStudentById(expectedStudentId));
            assertNotNull(actual);
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.getFirstName(), actual.getFirstName());
            assertEquals(expected.getLastName(), actual.getLastName());
            assertEquals(expected.getMidName(), actual.getMidName());
        }

        @Test
        void getStudentById_Throws_RuntimeException(){
            // given
            long studentId = 3L;
            RuntimeException expectedException = new RuntimeException("test exception");
            Mockito.when(mockStudentRepository.getStudentById(studentId))
                    .thenThrow(expectedException);
            // when
            RuntimeException actualException = assertThrows(RuntimeException.class, () -> studentService.getStudentById(studentId));
            assertNotNull(actualException);
            assertEquals(expectedException.getMessage(), actualException.getMessage());
        }
    }
}