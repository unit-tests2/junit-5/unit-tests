package com.example.base;

import com.example.extension.MyMockitoExtension;
import com.example.extension.MyTestAuditLogExtension;
import com.example.extension.MyTestExecutionTimeExtension;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith( value = {
        /*
        The order of this is as per the index XyzExtension declared below
        i.e.
        MyMockitoExtension is declared at index 0 it will call
        | step1 - beforeEach | step6 - afterEach
        MyTestAuditLogExtension is declared at index 1 it will call
        | step2 - beforeTestExecution | step5 - afterTestExecution
        MyTestExecutionTimeExtension is declared at index 2 it will call
        | step3 - beforeTestExecution | step4 - afterTestExecution
         */
        MyMockitoExtension.class,
        MyTestAuditLogExtension.class,
        MyTestExecutionTimeExtension.class
})
public interface MyExtendsWithExtension {
}
