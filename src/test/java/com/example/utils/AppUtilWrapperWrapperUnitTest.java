package com.example.utils;

import com.example.base.MyExtendsWithExtension;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.Objects;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppUtilWrapperWrapperUnitTest implements MyExtendsWithExtension {
    @BeforeAll
    void beforeAll(){
        System.out.println("initializing static mocks...");
        /*
        this requires mockito-inline dependency ref pom.xml
         */
        mockedAppUtils = Mockito.mockStatic(AppUtil.class);
        System.out.println("initializing static mocks success...");
    }

    @AfterAll
    void afterAll(){
        if(Objects.isNull(mockedAppUtils)){
            return;
        } else if (mockedAppUtils.isClosed()){
            return;
        }
        System.out.println("closing static mocks...");
        mockedAppUtils.close();
        System.out.println("closed static mocks success...");
    }

    private MockedStatic<AppUtil> mockedAppUtils;

    @InjectMocks
    private AppUtilWrapperWrapper appUtilWrapperWrapper;

    @Nested
    class GetStringUnitTest{

        @DisplayName(value = "mocking and calling via AppUtilWrapperWrapper in the unit case")
        @Test
        void getString_Via_AppUtilWrapperWrapper() {
            String expected = "getString_Via_AppUtilWrapperWrapper";
            mockedAppUtils.when(() -> AppUtil.getString()).thenReturn(expected);
            String actual = Assertions.assertDoesNotThrow(() -> appUtilWrapperWrapper.getString());
            Assertions.assertNotNull(actual);
            Assertions.assertEquals(expected, actual);
        }

    }
}