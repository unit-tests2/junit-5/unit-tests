package com.example.utils;

import com.example.base.MyExtendsWithExtension;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.Objects;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppUtilUnitTest implements MyExtendsWithExtension {

    private MockedStatic<AppUtil> mockedAppUtils;

    @BeforeAll
    void beforeAll(){
        System.out.println("initializing static mocks...");
        /*
        this requires mockito-inline dependency ref pom.xml
         */
        mockedAppUtils = Mockito.mockStatic(AppUtil.class);
        System.out.println("initializing static mocks success...");
    }

    @AfterAll
    void afterAll(){
        if(Objects.isNull(mockedAppUtils)){
            return;
        } else if (mockedAppUtils.isClosed()){
            return;
        }
        System.out.println("closing static mocks...");
        mockedAppUtils.close();
        System.out.println("closed static mocks success...");
    }

    @Nested
    class GetStringTest {
        @DisplayName(value = "mocking and calling directly in the unit case")
        @Test
        void getString_inside_test_case() {
            String expected = "getString_inside_test_case";
            mockedAppUtils.when(() -> AppUtil.getString()).thenReturn(expected);
            String actual = Assertions.assertDoesNotThrow(() -> AppUtil.getString());
            Assertions.assertNotNull(actual);
            Assertions.assertEquals(expected, actual);
        }
    }
}