package com.example.extension;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
This class is just a extension of MockitoExtension
Just to PRINT extra logs
 */
public class MyMockitoExtension extends MockitoExtension {

    private static final Logger logger = LoggerFactory.getLogger(MyMockitoExtension.class);
    @Override
    public void beforeEach(ExtensionContext context) {
        logger.info("step11111111111111111111111111111111111111111111111111111111111111111111111");
        super.beforeEach(context);
    }

    @Override
    public void afterEach(ExtensionContext context) {
        logger.info("step66666666666666666666666666666666666666666666666666666666666666666666666");
        super.afterEach(context);
    }
}
