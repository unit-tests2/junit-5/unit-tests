package com.example.extension;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/*
Custom mockito extension to log  duration of each test execution
 */
public class MyTestExecutionTimeExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    private final static Logger logger = LoggerFactory.getLogger(MyTestExecutionTimeExtension.class);
    private static final String START_TIME = "MY_START_TIME";
    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {
        logger.info("step33333333333333333333333333333333333333333333333333333333333333333333333");
        logger.info("################################################################## start");
        getStore(extensionContext).put(START_TIME, System.currentTimeMillis());
    }
    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        logger.info("step44444444444444444444444444444444444444444444444444444444444444444444444");
        Method testMethod = extensionContext.getRequiredTestMethod();
        long startTime = getStore(extensionContext).remove(START_TIME, Long.class);
        long duration  = System.currentTimeMillis() - startTime ;
        String testResult = extensionContext.getExecutionException().isPresent() ? "FAIL" : "PASS" ;
        logger.info("\n"+"""
                #############################################**{}**##################### start
                Test Result : {}
                Test Method : {}
                Total Time taken : {}
                Time Unit : TimeUnit.MILLISECONDS
                ################################################################## end
                """, getClass().getSimpleName(),testResult, testMethod.getName(), duration);
    }

    private ExtensionContext.Store getStore(ExtensionContext extensionContext){
        return extensionContext.getStore(ExtensionContext.Namespace.create(getClass(), extensionContext.getRequiredTestMethod()));
    }
}
