package com.example.extension;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

public class MyTestAuditLogExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    private static final Logger logger = LoggerFactory.getLogger(MyTestAuditLogExtension.class);

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {
        logger.info("step22222222222222222222222222222222222222222222222222222222222222222222222");
        Method method = extensionContext.getRequiredTestMethod();
        logger.info("\n" + """
                ----------------------**{}**---start-of-test--------
                started the execution of test : {}
                ------------------------------------------------
                """, getClass().getSimpleName(), method.getName());
    }

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        logger.info("step55555555555555555555555555555555555555555555555555555555555555555555555");
        Method method = extensionContext.getRequiredTestMethod();
        String testResult = extensionContext.getExecutionException().isPresent() ? "FAIL" : "PASS";
        logger.info("\n" + """
                ----------------------**{}**---end-of-test----------
                completed the execution of test : {}
                test result : {}
                ------------------------------------------------
                """, getClass().getSimpleName(), method.getName(), testResult);
    }
}
