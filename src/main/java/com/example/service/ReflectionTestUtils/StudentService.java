package com.example.service.ReflectionTestUtils;

import com.example.dto.Student;
import com.example.repository.StudentRepository;

public class StudentService {

    private final StudentRepository studentRepository;
    public StudentService(StudentRepository studentRepository) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + this.getClass().getSimpleName() + " | Constructor(StudentRepository)");
        this.studentRepository = studentRepository;
    }

    public Student getStudentById(Long id){
        return  studentRepository.getStudentById(id);
    }
}
