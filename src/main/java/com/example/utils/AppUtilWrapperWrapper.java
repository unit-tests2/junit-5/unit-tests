package com.example.utils;

public class AppUtilWrapperWrapper {

    public AppUtilWrapperWrapper() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + this.getClass().getSimpleName() + " | Constructor()");
    }

    public String getString() {
        AppUtilWrapper appUtilWrapper = new AppUtilWrapper();
        return appUtilWrapper.getString();
    }
}
