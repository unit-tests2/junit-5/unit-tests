package com.example.repository;

import com.example.dto.Student;

import java.util.UUID;

public class StudentRepository {
    public StudentRepository() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + this.getClass().getSimpleName() + " | Constructor()");
    }

    public Student getStudentById(Long id) {
        Student student = new Student();
        student.setId(id);
        student.setFirstName(UUID.randomUUID().toString());
        student.setMidName(UUID.randomUUID().toString());
        student.setLastName(UUID.randomUUID().toString());
        return student;
    }
}
